def dfs(graph, start, goal):
    paths = [[start]]
    start_point = [start]
    while start_point:
        vertex = start_point.pop()
        path = paths.pop()
        for next in graph[vertex] - set(path):
            if next == goal:
                yield path + [next]
            else:
                start_point.append(next)
                paths.append([next] + path)


def shortest_path(graph, start, goal):
    try:
        return next(dfs(graph, start, goal))
    except StopIteration:
        return None


def connections(r, c, grid):
    cons = set()
    if location(r + 1, c, grid) != '%':
        up = (r + 1, c)
        cons.add(up)
    if location(r, c - 1, grid) != '%' and c - 1 >= 0:
        left = (r, c - 1)
        cons.add(left)
    if location(r, c + 1, grid) != '%':
        right = (r, c + 1)
        cons.add(right)
    if location(r - 1, c, grid) != '%' and r - 1 >= 0:
        down = (r - 1, c)
        cons.add(down)
    return (r, c), cons


def graph(grid):
    keys = []
    vals = []
    nodes = []
    for r, el in enumerate(grid):
        for c, item in enumerate(el[0]):
            nodes.append(connections(r, c, grid))
    for i in nodes:
        if len(i[1]) < 1:
            continue
        keys.append(i[0])
        vals.append(i[1])

    nodes_dict = dict(zip(keys, vals))

    return nodes_dict


def location(r, c, grid):
    for index, el in enumerate(grid):
        if index == r:
            for _index, item in enumerate(el[0]):
                if _index == c:
                    return item


if __name__ == "__main__":
    p_loc = tuple(map(int, input().split()))
    f_loc = tuple(map(int, input().split()))
    grid_size = tuple(map(int, input().split()))
    grid = [input().split() for _ in range(grid_size[0])]
    all_nodes = graph(grid)
    answer = list(shortest_path(all_nodes, p_loc, f_loc))
    answer.reverse()
    answer.append(answer[0])
    answer.pop(0)
    print(answer)

