Input Format:

    The first line contains 2 space separated integers which is the position of the PacMan.
    The second line contains 2 space separated integers which is the position of the food.
    The third line of the input contains 2 space separated integers indicating the size of the rows and columns respectively.
    
    This is followed by row (r) lines each containing column (c) characters.
    A wall is represented by the character '%' ( ascii value 37 ), PacMan is represented by UpperCase alphabet 'P' ( ascii value 80 ),
    empty spaces which can be used by PacMan for movement is represented by the character '-' ( ascii value 45 ) and food is represented by the character '.' ( ascii value 46 )
    
    
Sample Input:

    3 9  
    5 1  
    7 20  
    %%%%%%%%%%%%%%%%%%%%
    %--------------%---%  
    %-%%-%%-%%-%%-%%-%-%  
    %--------P-------%-%  
    %%%%%%%%%%%%%%%%%%-%  
    %.-----------------%  
    %%%%%%%%%%%%%%%%%%%%  

Sample Output:
    [(3, 9), (3, 10), (2, 10), (1, 10), (1, 11), (1, 12), (1, 13), (2, 13), (3, 13), (3, 14), (3, 15), (3, 16), (2, 16), (1, 16), (1, 17), (1, 18), (2, 18), (3, 18), (4, 18), (5, 18), (5, 17), (5, 16), (5, 15), (5, 14), (5, 13), (5, 12), (5, 11), (5, 10), (5, 9), (5, 8), (5, 7), (5, 6), (5, 5), (5, 4), (5, 3), (5, 2), (5, 1)]

